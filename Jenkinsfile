pipeline {
  agent any
  parameters {
    text(name: 'ECR_REPO_NAME', defaultValue: 'petclinic', description: 'The repository name of the app.')
    text(name: 'ECR_REPO_NAMESPACE', defaultValue: '951865328368.dkr.ecr.ap-southeast-1.amazonaws.com', description: 'The namespace part of the repository.')
    password(name: 'ECR_REPO_NAME_PASSWORD', defaultValue: '', description: 'The ECR password for the repository.')
    text(name: 'MYSQL_USER', defaultValue: '', description: 'The admin user for the database the application petclinic will use.')
    password(name: 'MYSQL_PASS', defaultValue: '', description: 'The admin password')
  }

  tools {
    maven 'Maven-3.9.1'
    jdk 'jdk-21-GA'
  }

  stages {
    stage("Increment Version") {
      steps {
        script {
          sh 'mvn build-helper:parse-version versions:set ' +
            '-DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.nextMinorVersion}.\\\${parsedVersion.buildNumber} ' +
            'versions:commit'

          def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
          env.VERSION = matcher[0][1]
          env.TAG_NAME = "${env.VERSION}-${env.BUILD_NUMBER}"
          env.IMAGE_NAME = "${params.ECR_REPO_NAMESPACE}/${params.ECR_REPO_NAME}:${env.TAG_NAME}"
        }
      }
    }

    stage("Build artifact for Java Maven Application") {
      steps {
        script {
          sh "./mvnw clean"
          sh "./mvnw package -Dmaven.test.skip.exec -Dspring-boot.run.profiles=mysql"
        }
      }
    }

    stage("Build and Push Docker image to AWS ECR") {
      steps {
        script {
          sh "envsubst < Dockerfile-template > Dockerfile"
          
          sh "docker build -t ${env.IMAGE_NAME} ."
          sh "rm Dockerfile"

          sh 'echo $ECR_REPO_NAME_PASSWORD | docker login --username AWS --password-stdin ' + "${params.ECR_REPO_NAMESPACE}"
          sh "docker push ${env.IMAGE_NAME}"
          sh "docker logout ${params.ECR_REPO_NAMESPACE}"
        }
      }
    }

    stage("Deploy new application version to EKS cluster") {
      steps {
        script {
          sh "aws eks update-kubeconfig --name petclinic_k8s_cluster"

          dir("k8s-app") {
            sh "envsubst < database-template.yaml | kubectl apply -f -"
            sh "kubectl create secret docker-registry ecr-repo-creds --docker-username=AWS --docker-password=" + '$ECR_REPO_NAME_PASSWORD'
            sh "envsubst < petclinic-template.yaml | kubectl apply -f -"
          }
        }
      }
    }

    stage("Commit Version Update") {
      steps {
        script {
          sshagent(credentials: ['gitlab-ssh']) {
            sh 'git config user.email sheenismhael.lim@outlook.com'
            sh 'git config user.name "sheenilim08"'

            sh 'git remote set-url origin git@gitlab.com:twwnana/capstoneprojects/capstone-proj01.git'
            sh 'git add pom.xml'
            sh 'git commit -m "ci: version bump - $VERSION-$BUILD_NUMBER"'
            sh 'git push origin HEAD:main'
          }
        }
      }
    }
  }
}